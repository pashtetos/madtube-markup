//= ../../node_modules/jquery/dist/jquery.js

//= ../../node_modules/swiper/dist/js/swiper.jquery.js

var swiper = new Swiper('.slider-container', {
    pagination: '.swiper-pagination',
    paginationClickable: true,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    spaceBetween: 30
});

$(document).ready(function(){
    $("#anchor").on("click","a", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1000);
    });
});
